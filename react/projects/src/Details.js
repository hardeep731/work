// import React from 'react';
// import data from './data';
// import '@coreui/coreui/dist/css/coreui.min.css'
// import ExportDetails from './ExportDetails';
// import moment from 'moment';
// import {
//   CInputCheckbox,
//   CDataTable,
//   CHeader,
// } from "@coreui/react";
// class Requisition extends React.Component {
//   constructor() {
//     super()
//     this.state = {
//       details: {},
//       Checkall:false,
//       Singlecheck: false,
//       selectedIndexes: []
//     }
//   }
//   CommonCheck = (e) => {
//     let isChecked = e.target.checked;
//     console.log("calling common check",isChecked)
//     if (isChecked) {
//       //by checking event checked this loop gets executed
//       let lenData = data.length
//       let allIndexes = []
//       let items=this.state.items
//       //here comparing each index with len data and pushing that into array
//       for(let i=0;i<lenData;i+=1){
//         allIndexes.push(i)
//       }
//       this.setState({
//         //selected indexes gives all indexes so that all gets selected
//         selectedIndexes:allIndexes,
//       },()=>{console.log(this.state.selectedIndexes)})
      
//     }
// //else selected indexes will be empty array
//     else {
//       this.setState({
//         selectedIndexes:[],
//       },()=>{console.log(this.state.selectedIndexes)})
//     }
//   }
//   singleCheck = (e) => {
//     let isChecked = e.target.checked;
//     let index = e.target.value
//     console.log("SingleCheck Event",index,isChecked)
// //if single check even checked the below loop triggers
//     if(isChecked){
//       console.log(this.state.selectedIndexes)
//       let newSelectedIndexes = []
//       //in main check we are comparing with datalength but here we are checkeng with the state of selectedIndexes
//       for(let i=0;i<this.state.selectedIndexes.length;i+=1){
//         //new selected indexes will be pushed into the selectedIndexes
//         newSelectedIndexes.push(this.state.selectedIndexes[i])
//       }
//       //parse int is used to convert all the string type index values to integer type 
//       newSelectedIndexes.push(parseInt(index))
//       this.setState({
//         selectedIndexes: newSelectedIndexes,
//       },()=>{console.log(this.state.selectedIndexes)})
//     }
//     else{
//       let newSelectedIndexes = this.state.selectedIndexes.filter(val=>val!=index)
//       //else we are giving a filtered selected indexes values not equal to index
//       this.setState({
//         selectedIndexes : newSelectedIndexes
//       },()=>{console.log(this.state.selectedIndexes)})
//     }

//   }
//   //below function is optional to call
//   areAllIndexesChecked=()=>{
//     //used to check all indexes
//     let indexSet = new Set(this.state.selectedIndexes)
//     console.log(indexSet,"index set")
//     return indexSet.size === data.length
//   }
//   Checkvalue=(e)=>{
//     console.log(this.state.details,"-------------")
//   }
//   fields = [
//     { key: 'sno', _style: { width: '10%' } },
//     { key: 'code', _style: { width: "10%" } },
//     { key: 'description', _style: { width: '10%' } },
//     { key: 'qty', _style: { width: '20%' } },
//     {
//       key: 'appr_uom',
//       label: 'appr',
//       _style: { width: '20%' }
//     },
//     { key: 'check', label: "checkbox", filter: false, _style: { width: '3%', textAlign: "center" } }
//   ]
//   handleFilter = (e) => {
//     console.log(e,"event calling")
//     const FilterData = e;
//     this.setState({
//       details: FilterData,
//     })
//     // console.log(this.state.selectedIndexes,"senbehblfebg")
//   }
//   render() {
//     return (
//       <>
//         <CHeader className='Header'>
//           <h2>Requisition Page</h2>
//         </CHeader>
//         <CDataTable
//           items={data}
//           onFilteredItemsChange={this.handleFilter}
//           fields={this.fields}
//           columnFilter
//           tableFilter
//           sorter
//           striped
//           pagination
//           columnHeaderSlot={{
//             'check':
//                 <CInputCheckbox checked={this.areAllIndexesChecked()} name="checkall" onChange={this.CommonCheck}></CInputCheckbox>
//           }}
//           scopedSlots={{
//             'check':
//               (item, index) => (
//                 <td>
//                   <CInputCheckbox name="SingleCheck" checked={this.state.selectedIndexes.includes(index,item)} value={index} onChange={this.singleCheck}></CInputCheckbox>
//                 </td>
//               )
//           }
//           }
//         />
//         {<ExportDetails csvData={this.state.details} fileName={`${data.requisitionNo}+${moment()}`} />}
//       </>
//     )
//   }
// }
// export default Requisition;
import React from 'react';
import data from './data';
import '@coreui/coreui/dist/css/coreui.min.css'
import ExportDetails from './ExportDetails';
import moment from 'moment';
import {
  CInputCheckbox,
  CDataTable,
  CHeader,
} from "@coreui/react";
class Requisition extends React.Component {
  constructor() {
    super()
    this.state = {
      details: {},
      Checkall:false,
      Singlecheck: false,
      selectedIndexes: []
    }
  }
  CommonCheck = (e) => {
    let isChecked = e.target.checked;
    console.log("calling common check",isChecked)
    if (isChecked) {
      //by checking event checked this loop gets executed
      let lenData = this.state.details.length
      let allIndexes = []
      let items=this.state.items
      //here comparing each index with len data and pushing that into array
      for(let i=0;i<lenData;i+=1){
        allIndexes.push(i)
      }
      this.setState({
        //selected indexes gives all indexes so that all gets selected
        selectedIndexes:allIndexes,
      },()=>{console.log(this.state.selectedIndexes)})
      
    }
//else selected indexes will be empty array
    else {
      this.setState({
        selectedIndexes:[],
      },()=>{console.log(this.state.selectedIndexes)})
    }
  }
  singleCheck = (e) => {
    let isChecked = e.target.checked;
    let index = e.target.value
    console.log("SingleCheck Event",index,isChecked)
//if single check even checked the below loop triggers
    if(isChecked){
      console.log(this.state.selectedIndexes)
      let newSelectedIndexes = []
      //in main check we are comparing with datalength but here we are checkeng with the state of selectedIndexes
      for(let i=0;i<this.state.selectedIndexes.length;i+=1){
        //new selected indexes will be pushed into the selectedIndexes
        newSelectedIndexes.push(this.state.selectedIndexes[i])
      }
      //parse int is used to convert all the string type index values to integer type 
      newSelectedIndexes.push(parseInt(index))
      this.setState({
        selectedIndexes: newSelectedIndexes,
      },()=>{console.log(this.state.selectedIndexes)})
    }
    else{
      let newSelectedIndexes = this.state.selectedIndexes.filter(val=>val!=index)
      //else we are giving a filtered selected indexes values not equal to index
      this.setState({
        selectedIndexes : newSelectedIndexes
      },()=>{console.log(this.state.selectedIndexes)})
    }

  }
  //below function is optional to call
  areAllIndexesChecked=()=>{
    //used to check all indexes
    let indexSet = new Set(this.state.selectedIndexes)
    console.log(indexSet,"index set")
    return indexSet.size === data.length
  }
  Checkvalue=(e)=>{
    console.log(this.state.details,"-------------")
  }
  fields = [
    { key: 'sno', _style: { width: '10%' } },
    { key: 'code', _style: { width: "10%" } },
    { key: 'description', _style: { width: '10%' } },
    { key: 'qty', _style: { width: '20%' } },
    {
      key: 'appr_uom',
      label: 'appr',
      _style: { width: '20%' }
    },
    { key: 'check', label: "checkbox", filter: false, _style: { width: '3%', textAlign: "center" } }
  ]
  handleFilter = (e) => {
    console.log(e,"event calling")
    const FilterData = e;
    console.log(e,"details")
    console.log(data,"data")
    this.setState({
      details: FilterData,
    })
    // console.log(this.state.selectedIndexes,"senbehblfebg")
  }
  render() {
    return (
      <>
        <CHeader className='Header'>
          <h2>Requisition Page</h2>
        </CHeader>
        <CDataTable
          items={data}
          onFilteredItemsChange={this.handleFilter}
          fields={this.fields}
          columnFilter
          tableFilter
          sorter
          dark
          striped
          pagination
          columnHeaderSlot={{
            'check':
                <CInputCheckbox checked={this.areAllIndexesChecked()} name="checkall" onChange={this.CommonCheck}></CInputCheckbox>
          }}
          scopedSlots={{
            'check':
              (item, index) => (
                <td>
                  <CInputCheckbox name="SingleCheck" checked={this.state.selectedIndexes.includes(index,item)} value={index} onChange={this.singleCheck}></CInputCheckbox>
                </td>
              )
          }
          }
        />
        {<ExportDetails csvData={this.state.details} fileName={`${data.requisitionNo}+${moment()}`} />}
      </>
    )
  }
}
export default Requisition;
